defmodule Fetcher.MixProject do
  use Mix.Project

  def project do
    [
      app: :fetcher,
      version: "0.1.0",
      elixir: "~> 1.14",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      escript: escript()
    ]
  end

  def escript do
    [
      main_module: Fetcher,
      name: "fetch"
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:optimus, "~> 0.3.0"},
      {:owl, "~> 0.5.1"},
      {:tabula, "~> 2.2"},
      {:floki, "~> 0.34.0"},
      {:tesla, "~> 1.4"},
      {:hackney, "~> 1.18"}
    ]
  end
end
