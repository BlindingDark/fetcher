# Fetcher

## How to use it

``` shell
# build docker image
docker build -f Dockerfile -t fetcher .
# enter the shell
docker run --rm -it -u 1000:1000 -v $(pwd)/output:/home/app/output --name fetcher fetcher

## in the containter
# run tests
mix test
# show help info
./fetch --help
# fetch
./fetch -o output www.google.com https://autify.com
# show metadata
./fetch --metadata -o output www.google.com https://autify.com

## in the host
ls ./output
> autify.com.html www.google.com.html
```

## Design

Functions under `lib/crawler/`, `lib/parser/`, and`lib/storage/` are all _pure function_.

`lib/fetcher.ex` --> program entry  
`lib/crawler.ex` --> fetch file from internet  
`lib/storage.ex` --> save file to disk  
