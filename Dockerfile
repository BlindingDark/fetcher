FROM elixir:1.14-alpine

RUN echo 1

ENV USER=app
ENV UID=1000
ENV GID=1000

RUN addgroup -g $GID $USER
RUN adduser \
    --disabled-password \
    --home "/home/$USER" \
    --ingroup "$USER" \
    --uid "$UID" \
    "$USER"

USER app

ENV HOME /home/app/
ENV MIX_ENV=prod

WORKDIR ${HOME}/

COPY --chown=app lib ./lib
COPY --chown=app test ./test
COPY --chown=app mix.exs .
COPY --chown=app mix.lock .

RUN mix local.hex --force
RUN mix local.rebar --force
RUN mix deps.get
RUN mix escript.build

CMD ["/bin/sh"]
