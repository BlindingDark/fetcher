defmodule CrawlerTest do
  alias Crawler.Http
  use ExUnit.Case

  test "should return links" do
    html = """
    <html xmlns="http://www.w3.org/1999/xhtml">
      <a href="/example.html">
      <img src="/example.jpg">
      <img src="/">
    </html>
    """

    assert {:ok, ["/example.jpg", "/example.html"]} == Http.links(html)
  end

  test "should return images" do
    html = """
    <html xmlns="http://www.w3.org/1999/xhtml">
      <a href="/example.html">
      <img src="/example.jpg">
      <img src="/">
    </html>
    """

    assert {:ok, images} = Http.images(html)
    assert 2 == length(images)
  end

  test "should return a links" do
    html = """
    <html xmlns="http://www.w3.org/1999/xhtml">
      <a href="/example.html">
      <img src="/example.jpg">
      <img src="/">
    </html>
    """

    assert {:ok, a} = Http.a(html)
    assert 1 == length(a)
  end
end
