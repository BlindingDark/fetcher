defmodule ParserTest do
  alias Parser.Url
  use ExUnit.Case

  test "should parse" do
    assert [ok: "https://example.com", ok: "http://example.com"] ==
             ["https://example.com", "http://example.com"]
             |> Enum.map(&Url.parse/1)
  end

  test "should use https if it don't have scheme" do
    assert {:ok, "https://example.com"} == Url.parse("example.com")
  end

  test "should return error if it don't have host" do
    assert {:error, "missing a host"} == Url.parse("http://")
  end
end
