defmodule Parser.Url do
  @doc """
  Url Parser
  """
  @spec parse(binary) :: {:error, binary()} | {:ok, binary}
  def parse(url) do
    case URI.parse(url) do
      %URI{scheme: nil} ->
        parse("https://" <> url)

      %URI{host: nil} ->
        {:error, "missing a host"}

      %URI{host: ""} ->
        {:error, "missing a host"}

      _ ->
        {:ok, url}
    end
  end

  def site(link) do
    link
    |> URI.parse()
    |> Map.put(:scheme, nil)
    |> Map.put(:port, nil)
    |> URI.to_string()
    |> String.replace_prefix("//", "")
    |> String.replace_suffix("/", "")
  end
end
