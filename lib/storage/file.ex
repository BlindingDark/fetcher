defmodule Storage.File do
  alias Parser.Url

  def link_to_path(link, "") do
    "#{Url.site(link)}.html"
  end

  def link_to_path(link, base) do
    String.replace_leading(link, base, "")
  end

  def update_links(body, path) do
    if String.ends_with?(path, [".svg", ".gif"]) do
      body
    else
      Crawler.Http.update_links(body)
    end
  end
end
