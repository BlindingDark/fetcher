defmodule Fetcher do
  @moduledoc """
  Main CLI entry
  """

  # fetch deps
  @deps 1

  @doc """
  CLI entry
  """
  def main(args) do
    %{
      unknown: urls,
      args: %{urls: url},
      flags: %{metadata?: metadata?},
      options: %{output: output}
    } =
      options()
      |> Optimus.parse!(args)

    :ets.new(:config, [:ordered_set, :named_table])
    :ets.insert(:config, {:output, output})

    # Optimus doesn't support multiple args, so we handle it in another parse.
    Enum.each(urls, &Optimus.parse!(options(), [&1]))

    urls = [url | urls] |> Enum.uniq()

    if metadata? do
      Storage.metadata(urls)
      |> Tabula.print_table(only: [:site, :num_links, :images, :last_fetch])
    else
      fetch(urls)
    end
  end

  def fetch(urls) when is_list(urls) do
    urls
    |> Task.async_stream(&fetch/1, timeout: :infinity)
    |> Stream.run()
  end

  def fetch(url) when is_binary(url) do
    Owl.Spinner.run(
      fn -> do_fetch([{url, ""}], @deps) end,
      labels: [
        ok: "Done #{url}",
        error: fn reason -> "Failed #{url} #{inspect(reason)}" end,
        processing: "Fetching #{url}..."
      ]
    )
  end

  defp do_fetch([], _) do
    :ok
  end

  defp do_fetch([{url, base_url} | urls], deps) do
    with {:ok, {body, links}} <- Crawler.fetch(Path.join([base_url, url])),
         :ok <- Storage.save(url, base_url, body) do
      links = links |> Enum.map(fn link -> {link, url} end)

      if deps == 0 do
        do_fetch(urls, deps)
      else
        do_fetch(urls ++ links, deps - 1)
      end
    end
  end

  defp options() do
    Optimus.new!(
      name: "fetch",
      description: "Fetch html and more",
      version: "0.0.1",
      author: "Henry Sun (henry.ym.sun@outlook.com)",
      about: "This is a project for learning, please do NOT use for production",
      allow_unknown_args: true,
      parse_double_dash: true,
      args: [
        urls: [
          value_name: "urls",
          help: "The urls to be fetched",
          required: true,
          parser: &Parser.Url.parse/1
        ]
      ],
      flags: [
        metadata?: [
          short: "-m",
          long: "--metadata",
          help: "Specifies whether to print metadata instead of fetch",
          multiple: false,
          default: false
        ]
      ],
      options: [
        output: [
          short: "-o",
          long: "--output",
          help: "Output DIR",
          multiple: false,
          parser: :string,
          default: ""
        ]
      ]
    )
  end
end
