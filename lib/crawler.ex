defmodule Crawler do
  alias Crawler.Http

  def get(url) do
    [{Tesla.Middleware.FollowRedirects, [max_redirects: 10]}]
    |> Tesla.client({Tesla.Adapter.Hackney, [recv_timeout: 30_000]})
    |> Tesla.get(url)
  end

  def fetch(url) do
    with {:ok, env} <- get(url),
         {:ok, body} <- Http.body(env),
         {:ok, links} <- Http.links(body) do
      {:ok, {body, links}}
    end
  end
end
