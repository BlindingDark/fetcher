defmodule Storage do
  alias Crawler.Http
  alias Parser.Url

  def metadata(urls) when is_list(urls) do
    Enum.map(urls, &metadata/1)
  end

  def metadata(url) do
    site = Url.site(url)
    path = path(url, "")

    with {:ok, html} <- File.read(path),
         {:ok, images} <- Http.images(html),
         {:ok, a} <- Http.a(html),
         {:ok, time} <- updated_at(url) do
      %{site: site, num_links: length(a), images: length(images), last_fetch: time}
    else
      _ ->
        %{site: site, num_links: "", images: "", last_fetch: ""}
    end
  end

  def save(url, base_url, body) do
    path = path(url, base_url)
    body = Storage.File.update_links(body, path)

    File.mkdir_p!(Path.dirname(path))

    case File.write(path, body) do
      :ok ->
        :ok

      {:error, reason} ->
        {:error, "#{base_url}/#{url} #{inspect(reason)}"}
    end
  end

  def path(url, base_url) do
    path = Storage.File.link_to_path(url, base_url)
    [output: output] = :ets.lookup(:config, :output)
    Path.join([output, path])
  end

  def updated_at(url) do
    path = path(url, "")

    with {:ok, %File.Stat{ctime: time}} <- path |> File.stat(),
         {:ok, ndt} <- NaiveDateTime.from_erl(time) do
      {:ok, Calendar.strftime(ndt, "%a %b %-d %Y %X UTC")}
    end
  end
end
