defmodule Crawler.Http do
  @spec body(Tesla.Env.t()) :: {:error, nonempty_binary} | {:ok, binary()}
  def body(%Tesla.Env{status: 200, body: body}) do
    {:ok, body}
  end

  def body(%Tesla.Env{status: status}) do
    {:error, "#{status} #{:httpd_util.reason_phrase(status)}"}
  end

  @spec links(binary) :: {:error, binary} | {:ok, list(binary())}
  def links(html) do
    with {:ok, document} <- Floki.parse_document(html) do
      src = document |> Floki.find("[src]") |> Floki.attribute("src")
      href = document |> Floki.find("[href]") |> Floki.attribute("href")

      links =
        (src ++ href)
        |> Enum.map(&String.trim/1)
        |> Enum.filter(&link_filter/1)
        |> Enum.uniq()

      {:ok, links}
    end
  end

  # only fetch resources with relative path
  # because we can get rest via internet when open it
  defp link_filter("") do
    false
  end

  defp link_filter("/") do
    false
  end

  defp link_filter(link) do
    %URI{path: link} == URI.parse(link)
  end

  @spec update_links(binary) :: binary
  def update_links(body) do
    case Floki.parse_document(body) do
      {:ok, html} ->
        html
        |> Floki.find_and_update(
          "[src]",
          fn
            {tag, attrs} ->
              {tag, update_link(attrs, "src")}
          end
        )
        |> Floki.find_and_update(
          "[href]",
          fn
            {tag, attrs} ->
              {tag, update_link(attrs, "href")}
          end
        )
        |> Floki.raw_html(pretty: true)

      _ ->
        body
    end
  end

  defp update_link(attrs, tag) do
    attrs
    |> Enum.map(fn
      {^tag, value} ->
        if link_filter(value) do
          {tag, ".#{value}"}
        else
          {tag, value}
        end

      other ->
        other
    end)
  end

  def images(html) do
    with {:ok, document} <- Floki.parse_document(html) do
      images = document |> Floki.find("img")
      {:ok, images}
    end
  end

  def a(html) do
    with {:ok, document} <- Floki.parse_document(html) do
      a = document |> Floki.find("a")
      {:ok, a}
    end
  end
end
